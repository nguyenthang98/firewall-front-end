import {toast} from "react-toastify"

const REGEX_OPTIONS = ["N", "G", "f", "p", "i", "m", "s", "x", "1", "2", "3"];
const Utils = {
    clone: (obj) => {
        return JSON.parse(JSON.stringify(obj));
    },
    toRegexForm: (str) => {
        if(!str || !str.length)
            return "//";
        else {
            if (str[0] !== "/") {
                str = `/${str}`;
            }
            const lastSlashIdx = str.lastIndexOf("/");
            if (lastSlashIdx < 0) {
                return `${str}/`;
            }
            const options = [];
            const invalids = [];
            for (let i = lastSlashIdx + 1; i < str.length; ++i) {
                if (REGEX_OPTIONS.includes(str[i])) {
                    if (!options.includes(str[i]))
                        options.push(str[i]);
                } else {
                    invalids.push(str[i]);
                }
            }
            if (invalids.length) {
                toast.error(`Invalid (POSIX REGEX) options: ${invalids.join()}. Valid otpions are: [N|G|f|p|i|m|s|x|1|2|3]`)
            }
            return `${str.slice(0, lastSlashIdx + 1)}${options.join("")}`
        }
    }
}
export default Utils;
