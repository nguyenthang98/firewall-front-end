import React, { Component } from "react"
import UserService from "../../services/UserService"
import FirewallService from "../../services/FirewallService"
import "./style.css"

class HomePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            namespaces: []
        }
    }

    componentDidMount() {
        FirewallService
            .getNamespaces()
            .then(namespaces => {
                this.setState({namespaces: namespaces || []})
            });
    }

    render() {
        const hasToken = UserService.getToken();
        return (<div>
            { hasToken
                ? ( <div className="container-fluid">
                        <h3 className="text-center">Namespaces</h3>
                        { this.state.namespaces.map((ns, i) => (
                            <a className="btn btn-dark col-6 col-sm-4 col-md-3" key={i} href={`/dashboard/${ns}`}>{ns}</a>
                        )) }
                    </div> )
                : 
                    ( <div style={{margin: "2em 0"}} className="center-align">
                        <a className="btn center-align" href="/login">
                            Login to to service
                        </a>
                    </div> )
            }
        </div>)
    }
}

export default HomePage
