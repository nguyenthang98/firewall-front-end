import React, { Component } from "react"
import "./style.css"
import { withRouter } from "react-router"
// import { toast } from "react-toastify"
import FirewallService from "../../services/FirewallService"
import FilterTable from "../FilterTable"
import Utils from "../../utils"

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.state[FirewallService.LAYER_2] = {rows: [], columns: [], selection: [], use: true};
        this.state[FirewallService.LAYER_3] = {rows: [], columns: [], selection: [], use: true};
        this.state[FirewallService.LAYER_4] = {rows: [], columns: [], selection: [], use: true};
        this.state[FirewallService.LAYER_7] = {rows: [], columns: [], selection: [], use: true};

        this.state["supported-layers"] = [FirewallService.LAYER_2, FirewallService.LAYER_3, FirewallService.LAYER_4, FirewallService.LAYER_7];

        console.log(this.props);
    }

    getTableConfigsFromRules(layerName, rules, _use) {
        return new Promise(res => {
            const rows = rules.map(rule => FirewallService.ruleToTableRow(layerName, rule));
            FirewallService.getTableColumnConfigs(this.props.match.params.ns, layerName)
                .then(columns => {
                    const use = _use ? true:false;
                    res({rows, columns, use});
                })
        })
    }

    componentDidMount() {
        FirewallService.getFirewall(this.props.match.params.ns)
            .then(res => {
                console.log(res);
                const data = res || {};
                Object.keys(data).forEach(layerName => {
                    this.getTableConfigsFromRules(layerName, data[layerName].rules, data[layerName].use)
                        .then(layerTableConfig => {
                            const config = {};
                            config[layerName] = layerTableConfig;
                            this.setState(config);
                        })
                })
            });

       FirewallService.listInterfaces(this.props.match.params.ns)
            .then(res => {
                console.log("Interfaces", res);
            })
    }

    onAddRules(layer, newRules) {
        console.log("on add rules", newRules);
        FirewallService.addRules(this.props.match.params.ns, layer, newRules)
            .then(res => {
                const rules = res.rules;
                this.getTableConfigsFromRules(layer, rules, res.use)
                    .then(tableConfigs => {
                        const config = {};
                        config[layer] = Object.assign(this.state[layer], tableConfigs);
                        this.setState(config);
                    })
            })
    }

    onChangeRules(layer, oldRules, newRules) {
        console.log("on change rules", oldRules, newRules);
        FirewallService.changeRules(this.props.match.params.ns, layer, oldRules, newRules)
            .then(res => {
                const rules = res.rules;
                this.getTableConfigsFromRules(layer, rules, res.use)
                    .then(tableConfigs => {
                        const config = {};
                        config[layer] = Object.assign(this.state[layer], tableConfigs);
                        this.setState(config);
                    })
            })
    }

    onDeleteRules(layer, delRules) {
        console.log("on delete rules", delRules);
        FirewallService.deleteRules(this.props.match.params.ns, layer, Object.values(delRules))
            .then(res => {
                const rules = res.rules;
                this.getTableConfigsFromRules(layer, rules, res.use)
                    .then(tableConfigs => {
                        const config = {};
                        config[layer] = Object.assign(this.state[layer], tableConfigs);
                        this.setState(config);
                    })
            })
    }

    onSelectionChange(layer, selection) {
        const layerConf = this.state[layer];
        Object.assign(layerConf, {selection});
    }

    deleteSelected(layer) {
        const selectedRowIndices = this.state[layer].selection;
        if (!selectedRowIndices || !selectedRowIndices.length) return;
        const layerConf = this.state[layer];
        const rows = Utils.clone(layerConf.rows);
        const delRules = selectedRowIndices.map(idx => FirewallService.tableRowToRule(layer, rows[idx]));
        layerConf.selection.length = 0;
        this.onDeleteRules(layer, delRules);
    }

    toggleLayer(layer) {
        FirewallService.toggleLayer(this.props.match.params.ns, layer)
            .then(res => {
                const rules = res.rules;
                this.getTableConfigsFromRules(layer, rules, res.use)
                    .then(tableConfigs => {
                        const config = {};
                        config[layer] = Object.assign(this.state[layer], tableConfigs);
                        this.setState(config);
                    })
            })
    }

    render() {
        return (
            <div>
                <div className="filter-table container-fluid">
                    {
                        this.state["supported-layers"].map(layer => (
                            <div className="card" key={layer}>
                                <h4 className="card-header">{FirewallService.getLayerLabel(layer)}</h4>
                                <div className="container-fluid row" style={{"marginTop": "1em"}}>
                                    <div className="col-2">
                                        <label className="switch">
                                            <input type="checkbox" checked={this.state[layer].use === true} onChange={() => this.toggleLayer(layer)} />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                    <div className="ml-auto">
                                        <button
                                            className="btn btn-danger"
                                            disabled={!this.state[layer].use}
                                            onClick={() => this.deleteSelected(layer)}
                                        >Delete Selected</button>
                                    </div>
                                </div>
                                {
                                    this.state[layer].use
                                    ? ( <FilterTable
                                        rows={this.state[layer].rows}
                                        columns={this.state[layer].columns}
                                        selection={this.state[layer].selection}
                                        layer={layer}
                                        onAddRules={(...args) => this.onAddRules(layer,...args)}
                                        onChangeRules={(...args) => this.onChangeRules(layer,...args)}
                                        onDeleteRules={(...args) => this.onDeleteRules(layer,...args)}
                                        onSelectionChange={(...args) => this.onSelectionChange(layer, ...args)}
                                    />)
                                    : (
                                        <div className="alert alert-light" style={{textAlign: "center"}} role="alert">
                                            Disabled
                                        </div>
                                    )
                                }
                            </div>
                        ))
                    }
                 </div>
            </div>
        )
    }
}

export default withRouter(Dashboard);
