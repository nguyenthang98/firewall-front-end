import React, { Component } from "react"
import { withRouter } from "react-router"
import UserService from "../../services/UserService"

class OpenStackAuthentication extends Component {
    constructor(props) {
        super(props);
        console.log(props.match);
        UserService.setToken(props.match.params.token);
        window.location.replace(`${window.location.origin}/dashboard/${props.match.params.ns}`);
    }

    render() {
        return ( <div>Redirecting...</div> )
    }
}

export default withRouter(OpenStackAuthentication)
