import React, { Component, Suspense } from "react";
import { BrowserRouter, Switch, Redirect, Route } from "react-router-dom"
import "./style.css"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.bundle"
import '@devexpress/dx-react-grid-bootstrap4/dist/dx-react-grid-bootstrap4.css'
import 'open-iconic/font/css/open-iconic.css'
import 'open-iconic/font/css/open-iconic-bootstrap.css'
import 'react-toastify/dist/ReactToastify.css'
import { ToastContainer, toast } from "react-toastify"
import HomePage from "../HomePage"
import Dashboard from "../Dashboard"
import LoginPage from "../LoginPage"
import NotFoundPage from "../NotFoundPage"
import OpenStackAuthentication from "../OpenStackAuthentication"
import UserService from "../../services/UserService"

class Main extends Component {
    constructor(props) {
        super(props);
        toast.configure();
    }

    render() {
        return (
            <div>
                <BrowserRouter>
                    <Suspense fallback={<div>Loading ...</div>}>
                        <Switch>
                            <Route path="/" exact component={HomePage} />
                            <Route path="/openstack/:ns/:token" exac component={OpenStackAuthentication} />
                            <Route path="/dashboard/:ns" exac component={Dashboard} />
                            <Route path="/login" exact>
                                {UserService.getToken() ? <Redirect to="/" />:<LoginPage />}
                            </Route>
                            <Route path="/notfound" exact component={NotFoundPage} />
                            <Redirect to="/notfound" />
                        </Switch>
                    </Suspense>
                </BrowserRouter>
                <ToastContainer />
            </div>
        )
    }
}
export default Main;
