import React, { Component } from "react"
import Utils from "../../utils"
import {
    PagingState, IntegratedPaging,
    FilteringState, IntegratedFiltering,
    SelectionState, IntegratedSelection,
    DataTypeProvider, EditingState
} from "@devexpress/dx-react-grid"
import {
    Grid, Table, TableColumnResizing, TableHeaderRow,
    PagingPanel, TableFilterRow, TableSelection,
    TableEditRow, TableEditColumn, TableFixedColumns
} from "@devexpress/dx-react-grid-bootstrap4"
import FirewallService from "../../services/FirewallService"

const SelectFormater = ({value}) => (
    <div> {value} </div>
)

const selectCleanState = {value: null, label: ""}
const SelectEditor = ({value, onValueChange, column, row}) => {
    const options = Utils.clone(column.options);

    const _onValueChange = (newVal) => {
        if (row && column.beforeEditValue) {
            column.beforeEditValue(row, newVal)
                .then(() => {
                    onValueChange(newVal);
                })
        } else {
            onValueChange(newVal);
        }
    }

    if (row === undefined) {
        // filtering state
        if (!options.find(o => o.value === null))
            options.splice(0, 0, selectCleanState)
    } else if (row && value === undefined) {
        // editing row
        if (options.length)
            _onValueChange(options[0].value)
    }

    return (
        <select className="form-control" value={value} onChange={event => _onValueChange(event.target.value)} >
            {options.map((o, i)=> (
                <option key={i} value={o.value}>
                    {o.label}
                </option>
            ))}
        </select>
    )
}
const SelectTypeProvider = props => (
    <DataTypeProvider
        formatterComponent={SelectFormater}
        editorComponent={SelectEditor}
        {...props}
    />
)

const CommandButton = ({
  onExecute, icon, text, hint, color,
}) => (
  <button
    type="button"
    className="btn btn-link"
    style={{ padding: 11 }}
    onClick={(e) => {
      onExecute();
      e.stopPropagation();
    }}
    title={hint}
  >
    <span className={color || 'undefined'}>
      {icon ? <i className={`oi oi-${icon}`} style={{ marginRight: text ? 5 : 0 }} /> : null}
      {text}
    </span>
  </button>
);

const AddButton = ({ onExecute }) => (
  <CommandButton icon="plus" hint="Create new row" onExecute={onExecute} />
);

const EditButton = ({ onExecute }) => (
  <CommandButton icon="pencil" hint="Edit row" color="text-warning" onExecute={onExecute} />
);

const DeleteButton = ({ onExecute }) => (
  <CommandButton
    icon="trash"
    hint="Delete row"
    color="text-danger"
    onExecute={() => {
        onExecute();
    }}
  />
);

const CommitButton = ({ onExecute }) => (
  <CommandButton icon="check" hint="Save changes" color="text-success" onExecute={onExecute} />
);

const CancelButton = ({ onExecute }) => (
  <CommandButton icon="x" hint="Cancel changes" color="text-danger" onExecute={onExecute} />
);

const commandComponents = {
  add: AddButton,
  edit: EditButton,
  delete: DeleteButton,
  commit: CommitButton,
  cancel: CancelButton,
};

const Command = ({ id, onExecute }) => {
  const ButtonComponent = commandComponents[id];
  return (
    <ButtonComponent
      onExecute={onExecute}
    />
  );
};

class FilterTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columnWidths: FirewallService.getColumnDefaultWidths(this.props.layer),
            selected: []
        }
    }

    commitChanges({added, changed, deleted}) {
        console.log("Add: ", added);
        console.log("Change: ", changed);
        console.log("Delete: ", deleted);
        if (added) {
            this.props.onAddRules(added.map(row => FirewallService.tableRowToRule(this.props.layer, row)));
        }
        if (changed) {
            const rowIdices = Object.keys(changed);
            const oldRules = {};
            rowIdices.forEach(idx => {
                oldRules[idx] = FirewallService.tableRowToRule(this.props.layer, Utils.clone(this.props.rows[idx]))
            })
            const newRules = {};
            rowIdices.forEach(idx => {
                newRules[idx] = FirewallService.tableRowToRule(this.props.layer, Object.assign(Utils.clone(this.props.rows[idx]), changed[idx]))
            })
            this.props.onChangeRules(oldRules, newRules);
        }
        if (deleted) {
            const rules = {};
            deleted.forEach(delIdx => {
                rules[delIdx] = FirewallService.tableRowToRule(this.props.layer, this.props.rows[delIdx])
            })
            this.props.onDeleteRules(rules)
        }
    }

    render() {
        return (
            <div className="card">
                <Grid rows={this.props.rows} columns={this.props.columns} >
                    <PagingState defaultPageSize={4} />
                    <SelectTypeProvider for={this.props.columns.filter(c => c.editType === "select").map(c => c.name)} />
                    {/* */}
                    <SelectionState defaultSelection={this.props.selection} onSelectionChange={(selected => this.props.onSelectionChange(selected))}/>
                    <IntegratedSelection />
                    {/**/}
                    <FilteringState />
                    <EditingState onCommitChanges={(...args) => this.commitChanges(...args)} />
                    <IntegratedFiltering />
                    <IntegratedPaging />
                    <Table />
                    <TableColumnResizing defaultColumnWidths={this.state.columnWidths} />
                    <TableHeaderRow />
                    <TableFilterRow />
                    <TableEditRow />

                    <TableSelection highlightRow showSelectAll />
                    <TableEditColumn
                        width={100}
                        showAddCommand showEditCommand showDeleteCommand commandComponent={Command}
                    />
                    <TableFixedColumns leftColumns={[TableEditColumn.COLUMN_TYPE]} rightColumns={["action"]}/>
                    <PagingPanel pageSizes={[4, 8, 0]} />
                </Grid>
            </div>
        )
    }
}

export default FilterTable;
