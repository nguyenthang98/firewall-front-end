import React, { Component } from "react"
import UserService from "../../services/UserService"
import "./style.css"

class LoginPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
           username: "",
           password: ""
        }
    }

    handleChangeUname(e) {
        this.setState({"username": e.target.value});
    }

    handleChangePwd(e) {
        this.setState({"password": e.target.value});
    }

    submitForm(e) {
        console.log(this.state);
        UserService.authenticate(this.state.username, this.state.password);
        e.preventDefault();
    }

    render() {
        return (
            <div className="login-form">
                <form onSubmit={(e) => this.submitForm(e)}>
                    <div className="form-wrapper login-form-wrapper">
                        <div className="form-row login-form-row">
                            <h3 className="title form-row-1">Login</h3>
                        </div>
                        <div className="form-row login-form-row">
                            <label >Username</label>
                            <input id="username" className="form-control" type="text" value={this.state.username} onChange={(e) => this.handleChangeUname(e)} />
                        </div>
                        <div className="form-row login-form-row">
                            <label >Password</label>
                            <input id="password" className="form-control" type="password" value={this.state.password} onChange={(e) => this.handleChangePwd(e)} />
                        </div>
                        <div className="form-row login-form-row from-center">
                            <input type="submit" value="Login" className="btn btn-light login-form-btn form-row-1" />
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default LoginPage
