import React, { Component } from "react"

class NotFoundPage extends Component {
    render() {
        return (
            <div>
                <h1>Sorry, the content you looking for is currently not available.</h1>
                Return <a href="/">Home</a>
            </div>
        )
    }
}

export default NotFoundPage
