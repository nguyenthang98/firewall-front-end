import APICallService from "./APICallService"
import { toast } from "react-toastify"

const UserService = {
    getToken: () => {
        return sessionStorage.getItem("token");
    },
    setToken: (token) => {
        if (token) {
            sessionStorage.setItem("token", token);
        } else {
            sessionStorage.removeItem("token");
        }
    },
    authenticate: function (uname, pwd) {
        APICallService.POST("/auth/login", { uname: uname, pwd: pwd })
            .then(res => {
                console.log(res);
                this.setToken(res.token);
                if (this.getToken())
                    window.location.reload();
                else {
                    toast.error("Authentication Error!!");
                }
            })
    }
}

export default UserService;
