import UserService from "./UserService"
// import axios from "axios"
import {toast} from "react-toastify"

const baseURL = `http://${window.location.hostname}:27017`;
const APICallService = {
    handleHTTPError: (res) => {
        return new Promise((resolve, reject) => {
            res.json()
                .then(data => {
                    if (data.code && data.code >= 300) {
                        // error
                        if (data.code === 401) {
                            window.location.replace(`${baseURL}/login`);
                        }
                        reject(new Error(`${data.name}: ${data.message}`));
                    } else {
                        resolve(data);
                    }
                })
        })
    },
    handleError: (err) => {
        toast.error(err.message);
    },
    GET: function(path) {
        return fetch(`${baseURL}${path}`, {
                method: "GET",
                headers: {
                    'Authorization': UserService.getToken(),
                    'Content-Type': 'application/json',
                },
            })
            .then(this.handleHTTPError)
            .catch(this.handleError)
    },
    POST: function(path, data) {
        return fetch(`${baseURL}${path}`, {
                method: "POST",
                cache: "no-cache",
                headers: {
                    "Authorization": UserService.getToken(),
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(this.handleHTTPError)
            .catch(this.handleError)
    },
    PUT: function(path, data) {
        return fetch(`${baseURL}${path}`, {
                method: "PUT",
                cache: "no-cache",
                headers: {
                    "Authorization": UserService.getToken(),
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
            .then(this.handleHTTPError)
            .catch(this.handleError)
    },
    DELETE: function(path, data) {
        return fetch(`${baseURL}${path}`, {
                method: "DELETE",
                cache: "no-cache",
                headers: {
                    "Authorization": UserService.getToken(),
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(data)
            })
            .then(this.handleHTTPError)
            .catch(this.handleError)
    }
}

export default APICallService;
