import APICallService from "./APICallService";
import Utils from "../utils";

const ACTION_OPTIONS = [ {value: "DROP", label: "DROP"}, {value: "REJECT", label: "REJECT"}, {value: "ACCEPT", label: "ACCEPT"} ];

const FirewallService = {
    LAYER_2: "l2",
    LAYER_3: "l3",
    LAYER_4: "l4",
    LAYER_7: "l7",
    getLayerLabel: function(layer) {
        switch (layer) {
            case this.LAYER_2:
                return "Datalink Layer Filtering";
            case this.LAYER_3:
                return "Network Layer Filtering";
            case this.LAYER_4:
                return "Transport Layer Filtering";
            case this.LAYER_7:
                return "Payload Filtering";
            default:
                return "Layer not found";
        }
    },
    listProtocols: function(nsname, layer) {
        return APICallService.GET(`/firewall/${nsname}/${layer}/protocols`);
    },
    listInterfaces: function(nsname) {
        return APICallService.GET(`/firewall/${nsname}/interfaces`);
    },
    getNamespaces: function() {
        return APICallService.GET("/firewall");
    },
    addRules: function(nsname, layer, rules) {
        return APICallService.POST(`/firewall/${nsname}/${layer}`, {"rules": rules});
    },
    changeRules: function(nsname, layer, old_rules, new_rules) {
        return APICallService.PUT(`/firewall/${nsname}/${layer}`, { old_rules, new_rules})
    },
    deleteRules: function(nsname, layer, rules) {
        return APICallService.DELETE(`/firewall/${nsname}/${layer}`, {rules})
    },
    toggleLayer: function(nsname, layer) {
        return APICallService.GET(`/firewall/${nsname}/${layer}/toggle`)
    },
    getFirewall: function (nsname, layer) {
        switch (layer) {
            case this.LAYER_2:
                return APICallService.GET(`/firewall/${nsname}/${layer}`);
            case this.LAYER_3:
                return APICallService.GET(`/firewall/${nsname}/${layer}`);
            case this.LAYER_4:
                return APICallService.GET(`/firewall/${nsname}/${layer}`);
            case this.LAYER_7:
                return APICallService.GET(`/firewall/${nsname}/${layer}`);
            default:
                return APICallService.GET(`/firewall/${nsname}`);
        }
    },
    ruleToTableRow: function(layer, rule) {
        switch (layer) {
            case this.LAYER_2:
                return {
                    "macSource": rule["mac"] ? rule["mac"]["mac-source"]:null,
                    // "interface": rule["interface"] ? rule["interface"]:"any",
                    // "inIf": rule["in-interface"] ? rule["in-interface"]:"any",
                    // "outIf": rule["out-interface"] ? rule["out-interface"]:"any",
                    "action": rule["target"],
                }
            case this.LAYER_3:
                return {
                    "protocol": rule["protocol"] ? rule["protocol"] : "any",
                    "srcAddr": rule["src"] ? rule["src"] : "any",
                    "dstAddr": rule["dst"] ? rule["dst"] : "any",
                    "action": rule["target"]
                }
            case this.LAYER_4:
                const protocol = rule["protocol"] || "any";
                const protocolConfig = rule[protocol] || {};
                const option = {};
                Object.keys(protocolConfig).forEach(key => {
                    if (key !== "sport" && key !== "dport") {
                        option[key] = protocolConfig[key];
                    }
                })
                return {
                    "protocol": protocol,
                    "sport": protocolConfig["sport"],
                    "dport": protocolConfig["dport"],
                    "option": JSON.stringify(option),
                    "action": rule["target"]
                }
            case this.LAYER_7:
                return {
                    "pattern": rule["string"]["string"],
                    "action": rule["target"]
                }
            default:
                return {};
        }
    },
    tableRowToRule: function(layer, row) {
        switch (layer) {
            case this.LAYER_2:
                return {
                    "mac": {
                        "mac-source": row["macSource"]
                    },
                    // 'interface': row['interface'],
                    // "in-interface": row["inIf"],
                    // "out-interface": row["outIf"],
                    "target": row["action"]
                }
            case this.LAYER_3:
                return {
                    "protocol": row["protocol"],
                    "src": row["srcAddr"],
                    "dst": row["dstAddr"],
                    "target": row["action"]
                }
            case this.LAYER_4:
                const protocol = row["protocol"];
                const rule = {
                    "protocol": protocol,
                    "target": row["action"],
                };
                rule[protocol] = {
                    "sport": row["sport"] ? row["sport"]:null,
                    "dport": row["dport"] ? row["dport"]:null
                };
                if (!rule[protocol]["sport"]) delete rule[protocol]['sport'];
                if (!rule[protocol]["dport"]) delete rule[protocol]['dport'];
                const option = JSON.parse(row["option"] || "{}");
                Object.keys(option).forEach(key => {
                    rule[protocol][key] = option[key];
                })
                return rule;
            case this.LAYER_7:
                const config =  {
                    "target": row["action"],
                    "string": {}
                }
                config["string"]["string"] = Utils.toRegexForm(row["pattern"]);
                config["string"]["algo"] = "regex";
                return config;
            default:
                return {};
        }
    },
    getTableColumnConfigs: function(nsname, layer) {
        return new Promise(res => {
            switch (layer) {
                case this.LAYER_2:
                    this.listInterfaces(nsname)
                        .then(interfaces => {
                            const ifOptions = interfaces.map(ifcfg => ({
                                "value": ifcfg.name,
                                "label": `${ifcfg.name} - ${ifcfg.ip}`
                            }));
                            ifOptions.splice(0, 0, {"value": null, "label": "any"});
                            res( [
                                { name: "macSource", title: "MAC Source" },
                                // { name: "interface", title: "Interface", editType: "select", options: Utils.clone(ifOptions) },
                                // { name: "inIf", title: "In Interface", editType: "select", options: Utils.clone(ifOptions) },
                                // { name: "outIf", title: "Out Interface", editType: "select", options: Utils.clone(ifOptions) },
                                { name: "action", title: "Action", editType: "select", options: Utils.clone(ACTION_OPTIONS) }
                            ]);
                        });
                    break;
                case this.LAYER_3:
                    this.listProtocols(nsname, layer)
                        .then(protocols => {
                            const pOptions = [
                                {"value": null, "label": "any"},
                                ...protocols.map(protocol => ({"value": protocol.protocol, "label": protocol.protocol}))
                            ]
                            res([
                                { name: "protocol", title: "Protocol", editType: "select", options: pOptions },
                                { name: "srcAddr", title: "Source Address"},
                                { name: "dstAddr", title: "Destination Address"},
                                { name: "action", title: "Action", editType: "select", options: Utils.clone(ACTION_OPTIONS) }
                            ]);
                        })
                    break;
                case this.LAYER_4:
                    this.listProtocols(nsname, layer)
                        .then(protocols => {
                            const pOptions = [
                                {"value": null, "label": "any"},
                                ...protocols.map(protocol => ({"value": protocol.protocol, "label": protocol.protocol}))
                            ]
                            res([
                                { name: "protocol", title: "Protocol", editType: "select", options: pOptions},
                                { name: "sport", title: "Source Port"},
                                { name: "dport", title: "Destination Port"},
                                { name: "option", title: "Options"},
                                { name: "action", title: "Action", editType: "select", options: Utils.clone(ACTION_OPTIONS) }
                            ]);
                        })
                    break;
                case this.LAYER_7:
                    res([
                        { name: "pattern", title: "Pattern" },
                        { name: "action", title: "Action", editType: "select", options: Utils.clone(ACTION_OPTIONS) }
                    ]);
                    break;
                default:
                    res([]);
            }

        })
    },
    getColumnDefaultWidths: function(layer) {
        switch (layer) {
            case this.LAYER_2:
                return [
                    { columnName: "macSource", width: 200 },
                    // { columnName: "interface", width: 200 },
                    // { columnName: "inIf", width: 200 },
                    // { columnName: "outIf", width: 200 },
                    { columnName: "action", width: 200 }
                ];
            case this.LAYER_3:
                return [
                    { columnName: "protocol", width: 200 },
                    { columnName: "srcAddr", width: 200 },
                    { columnName: "dstAddr", width: 200 },
                    { columnName: "action", width: 200 }
                ];
            case this.LAYER_4:
                return [
                    { columnName: "protocol", width:200 },
                    { columnName: "sport", width: 200 },
                    { columnName: "dport", width: 200 },
                    { columnName: "option", width: 200},
                    { columnName: "action", width: 200 }
                ]
            case this.LAYER_7:
                return [
                    { columnName: "pattern", width: 200},
                    { columnName: "action", width: 200 }
                ]
            default:
                return [];
        }
    }
}

export default FirewallService;
